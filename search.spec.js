describe('Search:', function() {

    beforeEach(function () {
        browser.get(browser.params.url);
    });

    afterEach(function () {
        browser.manage().deleteAllCookies();
    });

    it('should navigate to the cart and verify that it is empty', function(){

        var cartIcon = element(by.css('#gnav-account-cart-description'));
        cartIcon.click();

        var errorMassageEmptyCart = element(by.xpath('html/body/div[5]/div[1]/div/div[4]/div/h2'));
        expect(errorMassageEmptyCart.getText()).toBe('Ваша корзина пуста.');
    });


});