var helper = require('./helper');
var faker = require('faker');

describe('Protractor Demo App', function() {
//  it('should have a title', function() {
//    browser.get('http://juliemr.github.io/protractor-demo/');

//    expect(browser.getTitle()).toEqual('Super Calculator');
//  });

	beforeEach(function(){
        browser.get(browser.params.url);
	});

    afterEach(function(){
        browser.manage().deleteAllCookies();
    });

    var randFirstName = faker.name.firstName();
    var randLastName = faker.name.lastName();
    var email = faker.internet.email();
    var password = faker.internet.password();

	it('should have a title', function() {


		var title = 'Etsy.com | Shop for anything from creative people everywhere';
		
    expect(browser.getTitle()).toEqual(title);
  });


    it('should register', function() {



        var registerButton = element(by.id('register'));
        var firstNameField = element(by.id('first-name'));
        var lastNameField = element(by.id('last-name'));
        var emailField = element(by.id('email'));
		var passwordField = element(by.id('password'));
        var passwordRepeatField = element(by.id('password-repeat'));
        var usernameRegisterField = element(by.id('username'));
        //     var paswordExistingError = element(by.id('pasword-existing'));

       	var etsy_finds = element(by.id('etsy_finds'));
        var registerButtonPopUp = element(by.id('register_button'));

        registerButton.click();

        helper.waitUntilReady(firstNameField);
        firstNameField.sendKeys(randFirstName);
        helper.waitUntilReady(lastNameField);
        lastNameField.sendKeys(randLastName);
        helper.waitUntilReady(emailField);
        emailField.sendKeys(email);
        helper.waitUntilReady(passwordField);
        passwordField.sendKeys(password);
        helper.waitUntilReady(passwordRepeatField);
        passwordRepeatField.sendKeys(password);
        helper.waitUntilReady(usernameRegisterField);
        usernameRegisterField.sendKeys('usernameRegisterField');

    });

});
